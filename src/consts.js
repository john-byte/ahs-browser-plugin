export const KEYS_CTRL_LEFT = "ControlLeft";
export const KEYS_CTRL_RIGHT = "ControlRight";
export const KEYS_ALT_LEFT = "AltLeft";
export const KEYS_ALT_RIGHT = "AltRight";
export const KEYS_A = "KeyA";
export const KEYS_ENTER = "Enter";
