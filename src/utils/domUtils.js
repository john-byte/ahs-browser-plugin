const TRESHOLD = 30;

const OP_UPPER = 0;
const OP_DOWN = 1;
const OP_NEXT = 2;
const OP_STOP = 3;
const OPS = [OP_UPPER, OP_DOWN, OP_NEXT, OP_STOP];

const extensionNode = document.getElementById("jbextensions-ahs");

export const getRandomDOMNode = () => {
    let root = document.body;

    for (let i = 0; i < TRESHOLD; i++) {
        const op = (Math.random() * OPS.length) ^ 0;

        switch (op) {
            case OP_UPPER:
                if (root !== document.body) {
                    root = root.parentNode;
                }
                break;
            case OP_DOWN:
                if (root.childNodes && root.childNodes.length) {
                    const randChildId = (Math.random() * root.childNodes.length) ^ 0;
                    root = root.childNodes[randChildId];
                }
                break;
            case OP_NEXT:
                if (root !== document.body && root.nextSibling) {
                    root = root.nextSibling;
                } else if (root !== document.body && root.previousSibling) {
                    root = root.previousSibling;
                }
                break;
            case OP_STOP:
                return root !== extensionNode ? root : document.body;
        }
    }

    return root !== extensionNode ? root : document.body;
};