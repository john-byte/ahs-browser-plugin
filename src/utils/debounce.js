export const debounce = (fn, ms) => {
    let timer;
    return (...args) => {
        if (timer) {
            clearTimeout(timer);
            timer = null;
        };
        timer = setTimeout(() => {
            fn(...args);
            timer = null;
        }, ms);
    }
}