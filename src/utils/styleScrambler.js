import { getRandomDOMNode } from './domUtils';

const commonStyleProps = {
    'margin': {
        template: (x) => x + "",
        $type: 'number',
        values: [10, 200]
    },
    'padding': {
        template: (x) => x + "",
        $type: 'number',
        values: [10, 200],
    },
    'background-color': {
        template: (x) => {
            return `rgba(${x[0]}, ${x[1]}, ${x[2]}, ${x[3]})`;
        },
        $type: 'range',
        values: [
            [0, 255],
            [0, 255],
            [0, 255],
            [0, 100],
        ]
    },
    'width': {
        template: (x) => x + "",
        $type: 'number',
        values: [0, 200],
    },
    'height': {
        template: (x) => x + "",
        $type: 'number',
        values: [0, 200],
    },
};

const styleKeys = Object.keys(commonStyleProps);

export const styleRandomNode = () => {
    const node = getRandomDOMNode();
    if (!(node instanceof HTMLElement) || !(node instanceof SVGElement)) {
        node.remove();
        return;
    }
    
    const randStyleKey = (Math.random() * styleKeys.length) ^ 0;
    const randStyleProp = styleKeys[randStyleKey];
    const randStyleSettings = commonStyleProps[randStyleProp];
    if (randStyleSettings.$type === 'range') {
        const range = randStyleSettings.values.map((e) => {
            const min = e[0];
            const max = e[1];
            return (Math.random() * (max - min) + min) ^ 0;
        });
        
        node.style[randStyleProp] = randStyleSettings.template(range);
    } else {
        const min = randStyleSettings.values[0];
        const max = randStyleSettings.values[1];
        const val = (Math.random() * (max - min) + min) ^ 0;
        node.style[randStyleProp] = randStyleSettings.template(val);
    }
}
