// This is the last fallback
const DEFAULT_CONFIG = {
    "uniqueness": 10,
    "workPeriod": 72,
    "effects": {
        "domNodeDeletion": {
            "balance": 4,
            "data": null,
        },
        "domNodeSwap": {
            "balance": 4,
            "data": null,
        },
        "domNodeStyleChange": {
            "balance": 4,
            "data": null,
        },
        "tabCrash": {
            "balance": 4,
            "data": null,
        },
        /*
        "creppyBanner.example": {
            "balance": 8,
            "data": null,
        }
        */
    },
    sites: [
        "2ch.hk",
    ]
};

const globalStorage = window.JB_EXTENSIONS?.globalStorage;
const configKey = "ahs-config";

export const loadConfig = async () => {
    let config;

    try {
        if (!globalStorage) {
            throw "Global storage not found";
        }

        const rawConfig = await globalStorage.getItem(configKey);
        if (!rawConfig) {
            throw "Config in global storage DNE";
        }
        config = JSON.parse(rawConfig);
        localStorage.setItem(configKey, config);
        return config;
    } catch (e) {
        console.warn(e);
    }

    const rawConfig = localStorage.getItem(configKey);
    if (rawConfig) {
        config = JSON.parse(rawConfig);
    } else {
        config = DEFAULT_CONFIG;
    }

    return config;
}

export const saveConfig = async (config) => {
    const val = JSON.stringify(config);
    localStorage.setItem(configKey, val);

    try {
        if (!globalStorage) {
            throw "Global storage not found";
        }

        await globalStorage.setItem(configKey, val);
    } catch (e) {
        console.warn(e);
    }
}
