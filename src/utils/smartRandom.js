export class SmartRandom {
    _entityToMaxLen;
    _repeatInterval;
    _cap;

    _choiceWindow;
    _caret;
    _emittedCnt;

    /**
     * @param {Map<any, number>} entityToMaxLen 
     * @param {number} repeatInterval 
     * @param {number} cap 
     */
    constructor(entityToMaxLen, repeatInterval, cap) {
        this._entityToMaxLen = entityToMaxLen;
        this._repeatInterval = repeatInterval;
        this._cap = cap;
        this._init();
    }   

    _init() {
        this._choiceWindow = [];
        this._entityToMaxLen.forEach((v, k) => {
            this._choiceWindow.push({ val: k, bal: v });
        });

        this._caret = 0;
        this._emittedCnt = 0;
    }

    clearState() {
        this._init();
    }

    resumeState(choiceWindow, caret, emittedCnt) {
        this._choiceWindow = choiceWindow;
        this._caret = caret;
        this._emittedCnt = emittedCnt;
    }

    snapState() {
        return {
            // A cheap way to deep copy object
            choiceWindow: JSON.parse(JSON.stringify(this._choiceWindow)),
            caret: this._caret,
            emittedCnt: this._emittedCnt,
        };
    }

    *getIterator() {
        const r = this._repeatInterval;
        const choiceWindow = this._choiceWindow;
        const len = this._choiceWindow.length;
        const entityToMaxLen = this._entityToMaxLen;
        
        while (this._emittedCnt < this._cap) {
            if (this._caret >= len) {
                return;
            }

            // Choose and write
            const randId = (Math.random() * (len - this._caret) + this._caret) ^ 0;
            this._emittedCnt++;
            yield choiceWindow[randId].val;

            // Swap
            const t = choiceWindow[this._caret];
            choiceWindow[this._caret] = choiceWindow[randId];
            choiceWindow[randId] = t;

            // Contract balance
            choiceWindow[this._caret].bal--;

            // Reset back after period
            if (this._emittedCnt % r === 0) {
                for (let i = this._caret - 1; i >= 0; i--) {
                    const it = choiceWindow[i];
                    it.bal = entityToMaxLen.get(it.val);
                }
        
                // Swap
                const t = choiceWindow[0];
                choiceWindow[0] = choiceWindow[this._caret];
                choiceWindow[this._caret] = t;
        
                this._caret = 0;
            }

            if (choiceWindow[this._caret].bal === 0) {
                this._caret++;
            }
        }
    }
}