import { getRandomDOMNode } from './domUtils';

export const deleteRandomNode = () => {
    const node = getRandomDOMNode();
    node.remove();
}

export const swapRandomNodes = () => {
    const node1 = getRandomDOMNode();
    const node2 = getRandomDOMNode();

    const parentNode1 = node1.parentNode;
    const parentNode2 = node2.parentNode;

    parentNode1.removeChild(node1);
    parentNode1.appendChild(node2);

    parentNode2.removeChild(node2);
    parentNode2.appendChild(node1);
}