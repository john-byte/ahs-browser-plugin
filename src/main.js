import Main from './components/Main';

const containerId = "jbextensions-ahs";
let container = document.getElementById(containerId);
if (container) {
	console.warn(`You have conflicting instance with id=${containerId}. It will be removed`);
	container.parentNode.removeChild(container);
}
container = document.createElement('div');
container.id = "jbextensions-ahs";
document.body.appendChild(container);

const app = new Main({
	target: container,
});

export default app;