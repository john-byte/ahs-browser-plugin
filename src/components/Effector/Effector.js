import { deleteRandomNode, swapRandomNodes } from "../../utils/domScramblers";
import { styleRandomNode } from "../../utils/styleScrambler";
import { lag } from "../../utils/lagger";
import { SmartRandom } from "../../utils/smartRandom";

const globalStorage = window.JB_EXTENSIONS?.globalStorage;
const randomStateKey = "ahs-random-state";

class Effector {
    _random
    _symTable
    _sites
    
    constructor(random, sites) {
        this._random = random;
        this._sites = sites;
        this._initSymTable();
    }

    // TODO: init calls from config also
    _initSymTable() {
        const res = {
            "domNodeDeletion": {
                call: deleteRandomNode,
                data: null,
            },
            "domNodeSwap": {
                call: swapRandomNodes,
                data: null,
            },
            "domNodeStyleChange": {
                call: styleRandomNode,
                data: null,
            },
            "tabCrash": {
                call: lag,
                data: null,
            },
        };

        this._symTable = res;
    }

    static _getEffectToPeriod(config) {
        const res = new Map();
        for (const eff in config.effects) {
            res.set(eff, config.effects[eff].balance);
        }
        return res;
    }

    static async _loadState() {
        let rawState;
        try {
            rawState = await globalStorage.getItem(randomStateKey);
        } catch (_) {
            rawState = localStorage.getItem(randomStateKey);
        }

        return rawState ? JSON.parse(rawState) : undefined;
    }

    static async fromStorage(config) {
        const effectsBalances = Effector._getEffectToPeriod(config);
        const random = new SmartRandom(effectsBalances, config.uniqueness, config.workPeriod);
        const state = await Effector._loadState();
        if (state) {
            random.resumeState(state.choiceWindow, state.caret, state.emittedCnt);
        }
        
        return new Effector(random, config.sites);
    }

    run() {
        let iterator = this._random.getIterator();
        // TODO: define interval for effect occurence in config
        const minMsec = 1000 * 15;
        const maxMsec = 1000 * 60; 

        const randMs = (Math.random() + (maxMsec - minMsec) + minMsec) ^ 0;
        const fn = () => {
            const origin = window.location.origin;

            if (this._sites.includes(origin)) {
                let effect = iterator.next();
                if (effect.done) {
                    this._random.clearState();
                    iterator = this._random.getIterator();
                    effect = iterator.next();
                }
                
                const actor = this._symTable[effect.value];
                actor.call(actor.data);

                this._saveState();
            }

            const randMs = (Math.random() + (maxMsec - minMsec) + minMsec) ^ 0;
            setTimeout(fn, randMs);
        };
        setTimeout(fn, randMs);
    }

    async _saveState() {
        const state = this._random.snapState();
        try {
            await globalStorage.setItem(randomStateKey, JSON.stringify(state));
        } catch (_) {
            localStorage.setItem(randomStateKey, JSON.stringify(state));
        }
    }
}

export default Effector;
